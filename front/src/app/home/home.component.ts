import { Component, OnInit, OnDestroy } from '@angular/core';

import { HttpService } from '../services/http.service';
import { Models } from '../services/models';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {

  models: Models;

  constructor( private httpService: HttpService, private mdl: Models ) {
    this.models = mdl;
  }

  ngOnInit(): void {
    this.getGridMap();
  }

  getObjectParams(): void {
    this.httpService.getObjects()
      .subscribe(result => {
        alert(JSON.stringify(result));
      }, error => {
        console.log(error);
      });
  }

  getGridMap(): void {
    this.httpService.getGrids()
      .subscribe(result => {
        this.models.gridArray = result;
      }, error => {
        console.log(error);
      });
  }

  createObject(SquareTop: number, SquareLeft: number): void {
    this.models.objectBody.SquareTop = SquareTop;
    this.models.objectBody.SquareLeft = SquareLeft;
    this.httpService.postCreateObj()
      .subscribe(result => {
      console.log(result);
    }, error => {
      console.log(error);
      alert('error');
    });
  }


  ngOnDestroy(): void {

  }

}



