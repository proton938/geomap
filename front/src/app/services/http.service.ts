import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpErrorResponse, HttpResponse} from '@angular/common/http';
import { ApiParams } from './ApiParams';
import { Models } from './models';
import { Grid } from './interfaces';
import {Observable} from 'rxjs';

@Injectable()
export class HttpService{

  apiParams: ApiParams;
  models: Models;

  constructor(private http: HttpClient, api: ApiParams, models: Models ){
    this.apiParams = api;
    this.models = models;
  }

  getObjects(): Observable<any> {
    return this.http.get<Grid>('http://localhost:2000/api/' + this.apiParams.objPath);
  }

  getGrids(): Observable<any> {
    return this.http.get<Grid>('http://localhost:2000/api/' + this.apiParams.gridPath);
  }

  postCreateObj(): Observable<any> {
    return this.http.post('http://localhost:2000/createobj', this.models.objectBody);
  }

}

