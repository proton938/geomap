
export interface Grid {
  Grid: Rows[];
}

export interface  Rows {
  Row: Squares[];
}

export interface Squares {
  Square: number[];
}

export interface ObjectParams {
  SquareTop: number;
  SquareLeft: number;
}
